---
layout: handbook-page-toc
title:  Gitlab Laptop Delivery Metrics
---
## On this page
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

## Gitlab Laptop Delivery Metrics

### Purpose

We're designing this page to publicly and transparently show our metrics around laptop shipping, delivery, and procurement. This page is in its early stage and we will iterate and add more content as we continue to advance our metrics. 

### Table Breakdown

The **Regions** section is detailing what region and what metric we are tracking, within we are reporting total laptops delivered per region and percentage of on time delivery for the related month. The acronym ROW stands for **Rest of the World**, these are region that we hire in but do not have a registered vendor or shipping entity. Please note these numbers do not reflect new hires or team members that opt to self-procure their own laptop. 


 
| Regions                         | November | December | January | February | March | April | May |
| -------------                   |:--------:|:--------:|:--------|:---------|:------|:------|:----|
| EMEA Laptops delivered          |20        |9         |6        |5         |5      |5      |10   |
| EMEA % on time                  |90%       |99.90%    |83.40%   |100%      |80%    |80%    |100% |
| North America laptops delivered |29        |18        |7        |13        |13     |11     |16   |
| North America % on time         |100%      |83.49%    |100%     |84.70%    |100%   |81.9   |82%  |
| APAC Laptops delivered          |4         |1         |5        |2         |3      |1      |4    |
| APAC % on time                  |100%      |100%      |80%      |100%      |67%    |100%   |100% |
| LATAM Laptops delivered         |0         |0         |0        |0         |0      |0      |0    |
| LATAM % on time                 |N/A       |N/A       |N/A      |N/A       |N/A    |N/A    |N/A  |
| ROW Laptops delivered           |0         |0         |0        |0         |0      |0      |0    |
| ROW % on time                   |N/A       |N/A       |N/A      |N/A       |N/A    |N/A    |N/A  |
| Total laptops delivered         |53        |29        |18       |20        |21     |19     |30   |
| % Laptops delivered on time     |96.23%    |86.30%    |88.90%   |90%       |91%    |84.3   |90%  |

