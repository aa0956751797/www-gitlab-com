---
layout: handbook-page-toc
title: "Relocation"
description: "GitLab's Policies & Processes in Relation to Team Member Relocations."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Relocation / Address Change

This process is managed by the People Connect team as the DRI.  For any questions not addressed below, please email [people-connect@gitlab.com](mailto:people-connect@gitlab.com). 

Moving to a new location is an exciting adventure, and at GitLab, we want to encourage team members to utilize the flexibility that our relocation policy offers. However, there are multiple aspects to consider including eligibility, possible impact on compensation (including equity and benefit eligibility) and the impact on your role at GitLab.  It’s important to know that any relocation will require business approval after considering the impact of a relocation on the departmental budget and the effectiveness of performing your role in the new location.  A relocation that results in no change in compensation can be approved by the team member's direct leader.  Relocations resulting in an increase in compensation must be approved by the team member's direct leader as well as an eGroup Member.  

### Importance of Communicating Location 

Team Members are required to communicate a relocation by following the short-term or long-term relocation process below prior to their move. The consequences of not communicating to the appropriate group could result in [underperformance](https://about.gitlab.com/handbook/leadership/underperformance/) management action or loss of employment, depending on the circumstances. 

Generally, team members may live or work outside their permanent residence for an aggregate amount of 183 days (6 months) per year.  However, the amount does depend on the particular country (or state) and what it considers to be a "resident" for tax purposes, as well as what it considers the tax year.  If you are not sure about the aggregate amount allowed by the country or state where you are visiting, you should check with your personal tax advisor. If you believe you or a team member you manage is at risk of exceeding (or has already exceeded) the aggregate amount of 183 days (6 months) (or less in certain countries), you must contact the People group **immediately**.

Having team members for 6 months or more working outside of their contractual location poses potential tax liability for both GitLab and the team member. For this reason, it is important to communicate transparently and promptly with the People Team.

### Short-Term Stay Definition

If you are not changing your permanent location (where you maintain permanent residency), but instead are traveling to different locations over a period of time, you are responsible for maintaining your health insurance, visas, and any other item directly relating to your travel.

Since any short-term stay is your decision and not required by GitLab as part of your role, you will not be eligible to use the [Business Accident Travel Policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#business-travel-accident-policy) or submit any expenses related to your travel. If you are hired in a role requiring a time zone alignment, you must still be able to fulfill that requirement.

If your short-term stay is for less than 6 months in a new location, there should be no need to update your address in Workday. 

If you plan on staying in a location for more than 6 months in a single calendar year, or are in that location for an aggregate of more than 6 months in a single calendar year, that becomes your residence and you must complete the long-term relocation process described below. In most countries, tax residency is determined by the place where a GitLab Team Member keeps its vital interests over the span of a calendar year. This is the place where you are registered, where your significant other (SO) and/or dependents live, the place where you work, where you live, where you keep your subscriptions etc.

** Please note that in some jurisdictions, even short term stays can add up to more than 183 days per year and can subject you to payroll tax in the new location.  Always make sure to check with your tax advisor before traveling to any location where you will be performing work for an extended period of time, whether it is more than or less than 183 days. 

### Long-Term Relocation

#### Long-Term Relocation Definition

A long-term relocation means that you will establish yourself in a new location. If you are spending more than six months in one calendar year in one location as part of an extensive period of travel and/or will have your mail delivered to an address in a different address, please follow the steps below to request a long-term relocation.

It is at the company's discretion to determine whether it can approve the relocation:
  - Some job positions require GitLab team members to be located in particular countries, locations, regions, or time zones.  For example, a salesperson hired to serve a specific region may need to stay in that region, or an engineer hired to respond to customer requests in a specific region/country.  
  - GitLab only allows relocations to countries in which we already have a formed entity.  Review our [Country hiring guidelines](/handbook/people-group/employment-solutions/#country-hiring-guidelines) to see if the country is eligible for a relocation. 
  - You can only work in countries where you can establish eligibility to work in the country without visa sponsorship provided by GitLab.
  - Relocation may also result in an adjustment to compensation, including equity eligibility, based on the location and the location factor. Depending on the location factor and the benefits offered in the particular country, the adjustment may result in an increase or a decrease to the compensation package. The Company retains discretion to determine whether it can support such an adjustment.  

At the onset, this practice may seem harsh when moving to a lower-paid location. One might argue that people should be paid the same regardless of where they live.  However, if you look at it from another angle and compare this practice to what most companies do, it should make more sense. For example, if you work for a company with physical locations and they require you to work out of an office or close to an office, you’d have to live in that area.  If you wanted to move to a location where they did not have a physical site, you would have no alternative but to resign and seek new employment in your new location. You would find that companies in the area pay at a locally competitive rate.

Now, let's say the company did have a site in your new location and they offered the flexibility to transfer. If they did not have a similar position open, you would have to either apply for a different open position in the same company or resign and apply externally (back to the realization that other companies will pay at a locally competitive rate). If you were lucky enough that they did have a similar role in the new location, a transfer would come with a pay rate based on the local market to ensure equity across all incumbents (people in the job) by location.

Adjusting [pay according to the local market in all cases](/handbook/total-rewards/compensation/) is fair to everyone. We can't remain consistent if we make exceptions to the policy and allow someone to earn more than locally competitive rates for the same work others in that region are performing (or will be hired to do). Being fair to all team members is not negotiable. It is a value we stand behind and take very seriously.

#### Considerations and Eligibility 

1. If you are considering applying for a long-term relocation to a new country, the first consideration is to ensure that GitLab has an [entity](/handbook/people-group/employment-solutions/#gitlab-entities-and-branches) in the country to which you would like to move.  We currently only support relocations to GitLab Entities that are open for hiring and do not have hiring restrictions or headcount caps.  This is in alignment with our [Country hiring guidelines](/handbook/people-group/employment-solutions/#country-hiring-guidelines).
1. Consider any changes to [benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/) as benefits can vary by country.
1. You must have the appropriate right to work documentation/visa requirements in the country that you are considering relocating to.
- Please note, at this moment GitLab only sponsors [relocations to the Netherlands](https://about.gitlab.com/handbook/people-group/visas/#right-to-immigrate-to-the-netherlands) and doesn't cover relocation costs for the team member's family members also looking to relocate.
1. You must have satisfied the required one year tenure to be eligible for relocation.  

Keep in mind, GitLab retains discretion at all times whether it can accommodate you to continue your role in the new location based on the requirements of your role and the potential impact on the business.  In some instances a move will not align to your proposed location, (e.g. a recruiter hired in EMEA to support EMEA would not be approved to move to the US), and in other instances the company may not be able to support a relocation in the proposed location. Second, in almost all situations the compensation, including equity eligibility, may change. During the relocation process, you will learn how your compensation may be impacted and be able to make an informed decision.  Any increases in compensation will need to be approved by your leader and by the E-Group member for your division.  This allows the business to validate budget availability early in the process. If you have a unique situation, please email [people-connect@gitlab.com](mailto:people-connect@gitlab.com). 


#### Eligibility to Work

If you are interested in a long-term relocation (defined above), you need to confirm that you would be eligible to work in that location. [Except for the Netherlands](/handbook/people-group/visas/#right-to-immigrate-to-the-netherlands), GitLab does not provide any form of sponsorship or immigration aid for team members who choose to move to new countries. For more information, please refer to our [Visa page](/handbook/people-group/visas). GitLab cannot assist or facilitate a process to help you become eligible to work in a location if you do not already have that eligibility. You are of course free to apply and gain that eligibility if there are steps you can take that do not involve GitLab.  Once you are certain that you are eligible to work in your requested location country, you will need to provide proof of eligibility by [uploading the proof in the Documents tab of your Workday profile](https://docs.google.com/document/d/19B0lsMu7dMhof1ghPuBxHP23DuDqi2qpWF8pCWyEUN4/edit). If you move before establishing eligibility to work, but then cannot establish eligibility to work in the new location within the first six months of residency, it may affect your ability to continue your role with GitLab.

## How To Apply for a Long-Term Relocation

### Team Member 

1. Review the [Country hiring guidelines](/handbook/people-group/employment-solutions/#country-hiring-guidelines) to see if we can support your relocation. 
  _If you are moving to a different country, please start this process no later than **3 months** prior to your ideal relocation date.  If your relocation is within the same country, please start the process no later than 30 days before your ideal relocation date._
1. If you are moving to a different country, upload eligibility documentation to Workday -  Per the [Eligibility to Work](https://about.gitlab.com/handbook/people-group/relocation/#eligibility-to-work) section above. 
1. Complete the [Relocation Request Form](https://docs.google.com/forms/d/1u8O-AvzAJh993GWJyVsm7C7EjMpV9S0tjaXYoce8F7o/viewform?edit_requested=true) to submit the initial request to start the process.  Upon completion, the form is sent automatically to People Connect who will review the details of your move and will work with the proper leaders and approvers to determine if the relocation can be approved (based on any budgetary and job duty impacts).  Your manager will review if your role can be performed adequately in the new location without negatively impacting stakeholders or customers.  Some positions require GitLab team members to be located in particular countries, locations, regions, or time zones and will not be eligible for relocation.  People Connect will work with your manager on the final decision.  Your manager will communicate the decision and the impact on your compensation so that you can make an informed final decision as to your move.  Your assigned People Business Partner will also be informed of your request to relocate. 
1. Start preparing and avoid a delay in your pay or change to your relocation effective date by ensuring you have all required work documentation prior to your relocation. Please take into account processing times for visa and other required appointments in the relocating country needed in order to receive a tax number, bank account, etc.

Note: Assuming your relocation is approved, any applicable changes in compensation will not be effective until your move date (or the date you start working in the new location), which will be listed as the effective date on your new contract. **We will not be able to process retroactive dated relocations. Please only use future dates when applying for your relocation.**

### Approvals Phase

Below are the steps to approve a long term relocation. 

#### Relocation Within Same Country and Same Location Factor (see Compensation Calculator to determine location factor)

No approval is needed.  Team Members can [update their address](https://docs.google.com/document/d/1NiNKWd-H9FTnYkmsWWi79-DGDSUCiWMfwFaGQPRnj14/edit) in the Contact tab in their Workday profile. A People Connect Team member will [approve the address in Workday](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/people_connect_team/#home-contact-changes---workday-inbox) and reach out if there are any additional steps necessary to complete the relocation process. 

#### Relocations To New Country or Different Location Factor (see Compensation Calculator to determine location factor)

1. Complete the [Relocation Request Form](https://docs.google.com/forms/d/1u8O-AvzAJh993GWJyVsm7C7EjMpV9S0tjaXYoce8F7o/viewform?edit_requested=true) to submit the initial request to start the process. Upon completion, the form is sent automatically to People Connect who will: 
1. Check
  - [Location Eligibility](https://comp-calculator.gitlab.net/hiring_status/index) Check the [Compensation calculator, country hiring status ](https://comp-calculator.gitlab.net/hiring_status/index) to confirm that the country is currently an entity country open to further hiring.  These are the only countries where team members can relocate.  
  - [Work Eligibility](https://about.gitlab.com/handbook/people-group/relocation/#eligibility-to-work)
1. Determine the impact on compensation based on the relocation utilizing the following practices:
  - If the country and the location factor don’t change, no change in compensation is allowed as part of the relocation action.  
  - If the country and/or the location factor changes, generally the compensation will be adjusted based on the location factor change.  For example, if a team member currently resides in a location with a location factor of .70 and the move results in the team member landing in a location with a location factor of .75, the compensation recommendation will be to increase the salary to position the team member’s salary to an equal position in the new range (compa-ratio).  If the team member currently resides in a location with a location factor of .80 and the move results in landing in a location with a location factor of .70, the compensation recommendation may be to decrease the salary to position the team member’s salary in an equal position in the new range.  Any changes in location factor by >.20 will require E-Group member approval due to the possible significant impact on compensation with a copy to the Division Director and VP to add context and transparency for E-Group member consideration.  Considerations in addition to budget availability include business criticality of the role, ability to perform the role from the proposed new location, and demonstrated performance by the team member.  

1. The People Connect Team member will update the [Relocation Tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit?ts=5e7a2c42#gid=972787144) with the details of the relocation and work with the manager to communicate the decision to the team member.  

### Once Approved

1. The People Connect Team member will inform the Manager once the relocation has been fully approved.
1.  The manager will convey to the team member that their relocation has been approved and relay their new compensation.  The manager will then determine if the team member still wishes to relocate based on the changes to compensation.  The manager will convey the team member's final decision to the People Connect team.  
1. The People Connect Team member will convert the approved compensation from the current currency to the local pay currency of the team member's new location, using the [rounding best practice](/handbook/total-rewards/compensation/compensation-calculator/#rounding-best-practice) method.  
_Ex. Team member is approved $50,000 USD as their new compensation and they are relocating to the Netherlands. Team members in the Netherlands are paid in Euro. Using the Jan 1, 2020 conversion, this converts to 44,577.20 Euro. Using the rounding best practice, the team member's comp will be 44,600 Euro.  
1. The People Connect Team member will ensure that all approval documents including original request from the team member and any approval email threads are saved in the team member's [Workday `Contracts & Changes` folder](https://docs.google.com/document/d/1ao_d_JxvqvZdqxlt4mBoHe1GcAhYT7B6YQoBgDxPdRE/edit). This approval will include the converted salary where applicable for the authorized signatory on the contract / adjustment letter to audit against before signing. 
1. The People Connect Team member will prepare a Relocation letter or new contract when applicable. 
1. Once the Relocation letter or contract is signed, the People Connect Team will process the changes in Workday and will inform the relevant Payroll team (uspayroll@gitlab.com or nonuspayroll@gitlab.com) for visibility regarding the upcoming changes.  
1. If the team member is relocating to a new country, the People Connect Team member will open a [relocation issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/onboarding_tasks/relocations.md) with any relevant on-/offboarding tasks

### Contract Phase 

The contract phase is unique based upon the team member's relocation. The different options are as follows; 
1. [Relocation within the Same Country ](https://about.gitlab.com/handbook/people-group/relocation/#relocation-within-the-same-country)
1. [Relocating to a new country ](https://about.gitlab.com/handbook/people-group/relocation/#relocation-to-a-new-country)

#### Relocation within the Same Country

##### Tasks for People Connect 

1. A Relocation letter is only needed if there is a compensation change. If there is no change in compensation, then the People Connect Team should direct the team member to update their address in the Contact tab in their Workday profile.

- Make a copy of the [Relocation letter](https://docs.google.com/document/d/18V9Qqlaetft8XMXLMPgbTFtZAGgoa7uiuN01-W38c6k/edit) and save it to the [working documents folder](https://drive.google.com/drive/folders/0B4eFM43gu7VPNUlKZEFtNGtSRk0?resourcekey=0-_qX_2OXbs71yJa3HZy0TAQ&usp=sharing).

- Enter all applicable information based on the approval email and the team member's Workday profile.

- Please note that the effective date of the change is the date of the expected move for the team member. If unsure, please confirm with the team member who is relocating. 

1. Once the Relocation letter has been created, ping the People Connect Team for auditing. 
1. Stage the letter in DocuSign and send for signature to the team member.  
1. Update the [Relocation Tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit?ts=5e7a2c42#gid=972787144) with the details of this relocation.
1. Upload the signed document to the team member's `Contracts & Changes` folder in Workday.
1. The People Connect Team member makes any necessary updates to the team member's Workday profile.
1. Once the team member updates their address in Workday, the People Connect Team member [approves the address change](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/people_connect_team/#home-contact-changes---workday-inbox) in Workday and if applicable updates the [locality](https://docs.google.com/document/d/1hpPikG0STncYKamaY8XlfMTwYdoszP-0Xogvpp5hyZ4/edit) and [benefit group](https://docs.google.com/document/d/1MuRjPnvK9PZI2kk58jHnMgC3iUQCzp7xdjsfQpJT37k/edit)
1. The People Connect Team member notifies Payroll of the changes by adding the relevant details to the applicable payroll sheet depending on the country under the 'Compensation' header and starting month tab.
1. Once the People Connect Team member has updated Workday, they will then reach out to another People Connect team member to audit the updated entry.

##### Tasks for the Team Member

1. [Update your address](https://docs.google.com/document/d/1NiNKWd-H9FTnYkmsWWi79-DGDSUCiWMfwFaGQPRnj14/edit) in the Contact tab in your Workday profile. Your [address will then get approved](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/people_connect_team/#home-contact-changes---workday-inbox) by the People Connect Team member team.

#### Relocation to a New Country

- Note: If the team member is relocating away from a location with PEO Employment, they must give notice to the PEO directly, once the relocation has been approved. Most [notice periods](https://about.gitlab.com/handbook/people-group/contracts-probation-periods/#probation-periods-of-team-members-employed-through-a-peo-or-an-entity) are usually 30 days for Employee:PEO, and 2 weeks for Contractor:PEO (CXC) but the team member must review their own contract, as it will be specified there.

- Note: If the team member is moving to the US, employment authorization (I-9) is required for legal purposes. The People Connect team will manage the [I-9 process](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/onboarding_process/#timing-of-i-9). 

##### Payroll set up (New Country)

- For team members relocating to a new country, it is possible there may be a delay as long as 6 weeks before their first payroll is disbursed. This is due to possible frequency changes, timing of the relocation (after the 10th of the month), availability of new government documents. It is important to communicate this with the relocating team member to ensure they prepare for this possible delay. 


##### Tasks for People Connect 

1. Create a new [contract](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/employment_contracts/) for the team member confirming new compensation (if applicable) and effective date.
- No stock options are given through relocation, so that line can be removed
- [Probation Period](https://about.gitlab.com/handbook/people-group/contracts-probation-periods/#probation-period): If a team member has continued service with GitLab (tenure with GitLab has been uninterrupted) and they have already passed the probationary period of their original location or contract, they do not need to go through the probation period of their new location or contract.
- A list of all signatories can be found [here](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/ces-contract-processes/#entity-contract-signatories). 
1. Once the contract has been created, ensure that all documentation of approvals of relocation has been [uploaded to the Contracts & Changes folder](https://docs.google.com/document/d/1ao_d_JxvqvZdqxlt4mBoHe1GcAhYT7B6YQoBgDxPdRE/edit) of the team member's Workday profile. 
1. If applicable: A [Mutual Termination Agreement](https://docs.google.com/document/d/1MJCWQupiqfU7rUk99qowHuxd64OPIHKfD05gLlfs7K8/edit) is needed, if the team member is relocating from IT BV.  
- A [Side letter Relocation - Transfer from one entity to another](https://docs.google.com/document/d/1UesnGAH1y0MMgWU37RRX2DuSP14mDLff/edit) is needed if the team member is relocating from one entity to another.
- Please note that a wet signature is necessary if the team member is relocating from GitLab GmbH (Germany).
- A resignation email (within notice period) from the team member to their current PEO is required, if the team member is relocating away from a location with [PEO Employment](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity-or-branch)
1. Ping a People Connect Team member for auditing in the `#connect-ops-team` private slack channel.
1. Stage the contract in DocuSign and send for signature first to the GitLab signatory and subsequently to the team member.
  - In the event that the team member requests any changes to the contract, once approved and updated, send an email to the team member with the breakdown of the applicable changes once the contract has been sent for signature via DocuSign.
1. Upload the signed document to the team member's `Contracts & Changes` folder in Workday.
  -If the team member is relocating to the Netherlands, please see the [Relocating To The Netherlands process](/handbook/people-group/relocation/#relocating-to-the-netherlands). 
1. The People Connect Team member makes any necessary updates to the team member's Workday profile following the [Change Job Aid](https://docs.google.com/document/d/1hpPikG0STncYKamaY8XlfMTwYdoszP-0Xogvpp5hyZ4/edit). Additional steps:
- Update the locality at point of relocation (don’t wait for the address change)
- Check if a future dated One-Time-Payment needs to get updated due to the currency change
- Set a reminder to update the PTO policy in BambooHR on the effective date of the relocation (until absences get managed via Workday)
- Set a reminder to [update benefit groups](https://docs.google.com/document/d/1MuRjPnvK9PZI2kk58jHnMgC3iUQCzp7xdjsfQpJT37k/edit) for US team members on the relocation effective date (we currently can’t future date this)
- If applicable due to a relocation to the Netherlands and a temporary contract: Click on ‘job change’ actions to ‘add contract’. Fill in start and end date, contract type and reason.
1. Once the People Connect Team member has updated Workday, they will then reach out to another People Connect team member to have them audit the updated entry.
1. The People Connect Team member notifies Total Rewards and Payroll of the changes by adding the relevant details to the applicable payroll sheet depending on the new country under the 'Compensation' header and starting month tab. 
- [Non-US payroll changes](https://docs.google.com/spreadsheets/d/1M_puIKHLWkHnVzOJ2MwjIpQ-mwxolae-LpT2VPlByv0/edit#gid=261391927)
- [(US)Payroll Changes](https://docs.google.com/spreadsheets/d/1163qL1tIG32vAVUNNIbhnPnzkL5euPAD9DGlOaapTjY/edit#gid=1431873926)
- [Canada Payroll Changes](https://docs.google.com/spreadsheets/d/1_jX0TuBarGHnvM3CL7n64ily9a7aV4himDREODk8Ddo/edit#gid=13386269)
1. The People Connect Team member will update the [Relocation Tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit#gid=972787144), that the relocation has been completed. 
1. People Connect Team member - Update the hiring manager and PBP in the original email thread that all tasks assigned to the DRI: People Connect Team member relating to the relocation process are complete, for clarity. Other teams, such as Total Rewards and Payroll may still be updating their systems.
- In the event that the team member decides not to proceed with the relocation, please delete the respective contracts from Google Drive, email and Workday for compliance and efficiency reasons.
1. People Connect Team Member checks whether there are any onboarding documents that need to be completed by the team member. You can refer to the existing [onboarding templates section](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/tree/master/.gitlab/issue_templates/onboarding_tasks) or the [Pre-Onboarding section](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/onboarding_process/#pre-onboarding) for more information on which countries will be applicable.
1. If a country does require onboarding documentation, create a [relocation issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/onboarding_tasks/relocations.md) for the team member using the applicable country template in the [team member epics](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues) and assign to yourself and the relocating team member. 
1. For US team members - Update the benefit group to INELIGIBLE and terminate the team member in PlanSource using the appropriate effective date.
1. If relocation is from the US to outside of the US, change the benefit group in Workday to INELIGIBLE and update PlanSource with the termination date and coverages to be terminated.
1. If the relocation is from outside the US to the US, place team member in the appropriate benefit group in Workday. Check if Team Member needs to process a qualifying life event if the employee is moving in or out of California. 
1. If the relocation is from outside the US to the US, please ensure the team member completes Section 1 of the I-9 on or before the first date of hire in the US and ensures that Section 2 of the I-9 is completed no later than three days after the date of hire in the US.
1. Check that any necessary changes to payroll and benefits administration are processed in time.


##### Tasks for Team Members - Relocation Approved  

Once your relocation has been approved but at least 30 days prior to relocation.  

1. If you are relocating away from a location with PEO Employment, you must give notice to the PEO directly, once the relocation has been approved. Most notice periods are usually 30 days, but please review your own contract, as it will be specified there.

Once your relocation has been approved and the contract has been signed. 

1. [Update your address](https://docs.google.com/document/d/1NiNKWd-H9FTnYkmsWWi79-DGDSUCiWMfwFaGQPRnj14/edit) in the Contact tab in your Workday profile.
1. Update your location on the [GitLab company team page](https://about.gitlab.com/company/team/).
1. [Update your National Identification Number](https://docs.google.com/document/d/1D1IX1AdouxU-0aUcfROuLQ5r2O_C8d_thBHEgCK5aQE/edit) and type in Workday under the Personal Data tab, if applicable.  
1. You may be requested to [update your banking information](https://docs.google.com/document/d/107-8rGMgGRgsHuz9ydKwq2UIaazI5ITnd0o8yDgDW8s/edit) in Workday.
1. If moving to a new country ensure to complete any tasks in the assigned relocation issue by the due date

### Country Specific Tasks 

#### The United States of America 
##### Relocating **To** The United States
1. Run the [Add Employee integration](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/onboarding_process/#steps-to-run-the-lawlogix-integration-in-workday) from Workday to LawLogix in order to intiate the I-9 process for the team member moving at least 5 days before the relocation. 
##### Relocating **From** The United States
1. If a team member is leaving the US, run the [Termination integration from Workday](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/onboarding_process/#steps-to-run-the-lawlogix-integration-in-workday) to terminate the team member from LawLogix (I-9). This will allow the I-9 to be purged at the correct time. 
1. Set yourself a reminder to [update the Benefit Group](https://docs.google.com/document/d/1MuRjPnvK9PZI2kk58jHnMgC3iUQCzp7xdjsfQpJT37k/edit?usp=sharing) in Workday to `INELIGIBLE` within one week of the relocation effective date. This confirms the end of the team member's US benefits eligiblity.

#### The Netherlands 
##### Relocating **To** The Netherlands
The steps our People Connect Team member team uses to support relocations to the Netherlands in addition to above [relocation process](/handbook/people-group/relocation/#how-to-apply-for-a-long-term-relocation):
1. The People Connect Team member stages the relocation contract via Docusign for the team member and GitLab signatory to sign and adds hr@savvy-group.eu to 'Receive a copy' once signed.
1. The People Connect Team member will then reach out to our Payroll vendor in the Netherlands by emailing hr@savvy-group.eu and introduce the relocating team member in cc to support them with their relocation.
1. The vendor will supply all the documentation needed to apply for relocation including information about: [30% Ruling Questionnaire](https://docs.google.com/document/d/1Ok6LS9T4P6tnPu2N6BDRDeveOYzd1ILpkbQRhl911w4/edit?ts=5caf1bca), [wage tax](https://drive.google.com/file/d/1q8N-idoYGFSCw2ajat9RscfQ004EL-PS/view) form and potentially an application for a BSN Number.
- **During COVID a letter stating your requirement to relocate to the Netherlands at such time is required, the People Connect team member will provide the vendor with such, using the following [template](https://docs.google.com/document/d/1yE7DrHBLOj0blBJwaOIC8f2bLMFS10wYUZvg9BqjnF4/edit?ts=5e592e30)** 
- **A completed health declaration form, the vendor will provide this to you**.
1. Details on that process can be found under the BSN number section on the [visas page](/handbook/people-group/visas/#bsn-number) in the handbook.
- **Once the approved Visa and 30% ruling is received by the team member, the team members should email this to PeopleOps for filing in Workday.**
1. Sometimes the BSN might not be ready for the first Payroll, if this happens team members can still be on the Dutch Payroll, it just means the highest rate of tax will apply for this period. Any overpaid tax can be claimed back via [a year end tax return](https://www.government.nl/topics/income-tax/filing-a-tax-return). When you receive your BSN please ensure to add to your Workday profile and email nonuspayroll@gitlab.com to let them know. Any Payroll queries should be directed to nonuspayroll@gitlab.com
1. The People Connect team completes [Tasks for People Connect](https://about.gitlab.com/handbook/people-group/relocation/#tasks-for-people-connect-1) as described in above relocation process.

** Review [best practices](https://about.gitlab.com/handbook/people-group/contracts-probation-periods/#netherlands-renewal-process) for The Netherlands contracts **

#### India
##### Relocating **from** India
Our PEO in India can only process terminations on working days (typically Monday to Friday), please take this into account when deciding on the relocation effective date.

#### Germany
##### Relocating **to** Germany 
1. Review current [onboarding tasks](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/onboarding_tasks/country_germany.md) for Germany and ensure necessary paperwork is filled out. 
##### Relocating **from** Germany 
- Relocation Contract and Mutual Termination Agreement Process for Germany

These are the steps our People Connect Team member team uses to send contracts to team members relocating to, or moving from Germany. Please note all German contracts require a wet signature.

Once the contract, tripartite agreement, or Mutual Termination Agreement (MTA) has been created by the People Connect Team member, next steps are to stage the documents in DocuSign, while also sending the documents to the signatories to have them physically sign and mail them. 

1. Stage the documents for signature in DocuSign. This document will be sent to the Statutory Director first and then to the team member.
  - Once signed, save the file into the team members Workday profile.
1. At the same time you stage the documents for signature in DocuSign, you will also need to email a copy of the document to both the Statutory Director as well as the team member.
  - In the body of the email, you will need to explain the below steps for the process.
  1. The Statutory Director will need to print two copies of the document. They will need to sign both of the copies, and then mail them to the team member. Please include the address and phone number of the team member in the email. 
  1. Once the team member receives the documents in the mail, they will keep one copy and then will need to sign and send the other copy to our German Counsel. The address for our German Counsel is listed in the PeopleOps 1password vault. Please include this address in the email.
  - The postage fees can be submitted for reimbursement. Please also include that information in the email.
  - It is also best practice to recommend in the email that the Statutory Director confirm that they have mailed the document.
1. Once the German Legal Counsel receives the documents, they will send a scanned copy of the signed documents to the People Connect team. This document will also need to be saved in the team members Workday profile. 

#### South Korea, France or Singapore
##### Relocating **to** South Korea, France or Singapore
1. The People Connect Lead shares the relocation contract with gitlabHRSS@globalupside.com. Global Upside then sends an Egnyte (payroll platform) login invitation email to the relocating team member. If they are relocating to France an invite to Mihi will also be sent.

## Reporting for Relocations
Access to to the 'Gitlab Team Member Relocations' Workday report can be requested via [this issue](https://gitlab.com/gitlab-com/people-group/people-tools-technology/general/issues/new?issuable_template=intake) and automatically sent on a regular basis. 

This report will include the following information: 
- Team Member Name
- Manager
- Relocation Date
- Starting Location
- Ending Location


#### Steps for a Canceled Relocation Request
In the event that the team member decides not to proceed with the relocation. Not all of these steps will be needed, depending on how far into the relocation process has taken place. 

1. Delete the respective contracts from Google Drive and Workday, if signed
1. Notify stakeholder of the change.  Possible stakeholders:
      - PEO 
      - Payroll: remove relocation details from the payroll sheets 
      - HRSavvy 
1. Workday - Update fields that were changed for the relocation.
1. Update [relocation tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit#gid=972787144) 
