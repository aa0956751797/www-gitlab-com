---
layout: markdown_page
title: "FY24-Q3 OKRs"
description: "View GitLabs Objective-Key Results for FY24 Q3. Learn more here!"
canonical_path: "/company/okrs/fy24-q3/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from 2023-08-01 to 2023-010-31.

The source of truth for FY24-Q3 OKRs will be in GitLab.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2023-06-26 | CEO shares top goals with E-group for feedback |
| -4 | 2023-07-03 | CEO shares top goals in #okrs Slack channel |
| -4 | 2023-07-03 | E-group propose OKRs for their functions in the OKR draft review meeting agenda |
| -3 | 2023-07-10 | E-group 50 minute draft review meeting | 
| -2 | 2023-7-17 | E-group discusses with their respective teams and polishes OKRs |
| -2 | 2023-07-17 | CEO top goals available in GitLab | 
| -1 | 2023-07-24 | Function OKRs are put into GitLab and links are shared in #okrs Slack channel |
| -1 | 2023-07-24 | CEO reports post links to final OKRs in #okrs slack channel and @ mention the CEO and CoS to the CEO for approval |
| 0  | 2023-07-31 | CoS to the CEO updates OKR page for current quarter to be active and includes CEO level OKRs with consideration to what is public and non-public |


## OKRs

The source of truth for GitLab OKRs and KRs is [GitLab](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=created_date&state=opened&type%5B%5D=key_result&label_name%5B%5D=CEO%20OKR&first_page_size=20). CEO objectives and KRs are captured on this page. 

### 1. CEO: 
1. **CEO KR**: 
1. **CEO KR**: 
1. **CEO KR**:

### 2. CEO: 
1. **CEO KR**: 
1. **CEO KR**: 
1. **CEO KR**:

### 3. CEO: 
1. **CEO KR**: 
1. **CEO KR**: 
1. **CEO KR**:

### 4. CEO: 
1. **CEO KR**: 
1. **CEO KR**: 
1. **CEO KR**:

### 5. CEO: 
1. **CEO KR**: 
1. **CEO KR**: 
1. **CEO KR**:
