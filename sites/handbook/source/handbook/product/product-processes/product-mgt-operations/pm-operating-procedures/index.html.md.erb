
layout: handbook-page-toc
title: Product Management Procedures
description: >-
  As a Product Organization, we work to create a flexible yet concise product
  development framework for developing products that customers love and value.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Product Management Operations  

This page details the meetings and working relationships. 

### Team Meetings

Below you'll find overviews for team meetings designed to boost Product team knowledge, communication, and collaboration. Our goal is to have minimal but valuable sync meetings for the Product team. All sync Product team meetings are required to have agendas and recordings that support async collaboration. We encourage any Product team member to propose a new meeting or the re-design of an existing meeting to help drive or improve collaboration amongst the Product team in an effort to drive more product value for our users. 

#### Adding or revising Product team meetings

To ensure transparency and alignment on the design and frequency of team meetings requiring time commitment from the entire or broader Product team, please raise an MR on this page, requesting review and collaboration from Product Operations, the CPO and any other DRIs noted for the meeting. Product Operations should be always be included to review the addition of new or major revision of existing team meetings because they monitor the overall flow, productivity and satisfaction of all Product team meetings. "Broader" Product team refers to meetings that affect team members across Sections, Stages, and Groups on a regular cadence. For example, if a meeting is proposed that requires the participation of **all** Group Product Managers on the Product team **every month**. 

The intent of this review and collaboration workflow is not to "block" but to help ensure team meetings compliment and flow well together, taking into consideration the following: 

- the existing macro Product team workflow 
- non-duplication of existing sync or async meetings
- support from Product Operations for successful rollout to and adoption by the Product team

#### Product Management Meeting (Bi-weekly)
**DRI:** [Product Operations](/company/team/#fseifoddini) and [Chief Product Officer](/company/team/#david)

- **What:** A bi-weekly team meeting for Product. All team members can add items to the agenda either as a Read-Only, or as a discussion item.
- **Who:** Product along with interested parties across departments. The meeting is open to all GitLab team members.
- **When:** Every other Tuesday for 50 minutes. The start time alternates each week between <time datetime="15:00">3 pm UTC (10 am ET / 7 am PT)</time> and <time datetime="21:00">9 pm UTC (4 pm ET / 1 pm PT)</time> to accommodate timezones.
- **Format:** The agenda is curated and moderated by Product Operations in partnership with the Chief Product Officer
- **Recordings:** The meetings are set as Private Recordings and are saved to the [Product Team](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko7HO427nXkoz9kovi_2dBi) playlist on YouTube Unfiltered. If you are unable to view a video, please ensure you are logged in as [GitLab Unfiltered](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube).

**Potential Topics:**

1. `READ-ONLY` announcements which require no synchronous discussion and are for information sharing only. If any action is required, this should be highlighted specifically by the reporter.
1. New hire announcements, bonuses, promotions and other celebrations.
1. Announcements and discussion on department-wide changes (e.g. OKRs, performance indicators, pricing changes).
1. Learning about the product: showcasing different product areas, their direction, why they matter to GitLab.
1. Iteration: helping your peers break down issues and designs into smaller steps, across the department.
1. **Guest Speakers:** We encourage product managers to nominate guest speakers by reaching out to [Product Operations](/handbook/product/product-operations/) directly with a request.

All product team members are encouraged to add agenda items. The Chief Product Officer will prioritize the discussion items, and any items we can't get to in the allotted 50 min will be moved to the following meeting. If you are a Product team member and are unable to attend the call, you may add items for `READ-ONLY` to the agenda.

#### Product Leadership Meeting (Weekly)
**DRI:** [Chief Product Officer](/company/team/#david) and  [EBA to Chief Product Officer](/company/team/#gschwam)

The Chief Product Officer and their direct reports track our highest priority Product Team initiatives. If one of those initiatives needs to be discussed synchronously the assigned individual should add it to the meeting's [GoogleDoc agenda](https://docs.google.com/document/d/1yN2n1ei24HiM5G7tBxaqK4yNPYreKpLZ3CG3nsEFOR4/edit#heading=h.384zmpcao5l6). Directors can delegate and coordinate cross-product initiatives as needed based on the issues in this board.

As part of this Product Leadership Meeting we also [review progress towards our OKRs](https://gitlab.com/gitlab-com/Product/issues/187).

Non-public items for discussion should be added directly to the [agenda document](https://docs.google.com/document/d/1yN2n1ei24HiM5G7tBxaqK4yNPYreKpLZ3CG3nsEFOR4/edit#heading=h.384zmpcao5l6) for the meeting.

#### Section Performance Indicator Review (Monthly)
**DRI:** Appropriate [Product Section](/handbook/product/categories) Leader & [Senior Director, Product Monetization](/company/team/#justinfarris)

- **What:** A monthly meeting for [Product  Sections](/handbook/product/categories/#devops-stages) to provide updates on  Performance Indicators, inclusive of their Stages and Groups and the cross-functional team. The main deliverable from the team is for each group to clearly show what they are doing in the next month to drive their metrics in the right direction.
- **When:** During the last two weeks of every month 
- **Duration:** 50 minutes
- **Format:** The meeting is lead by the Section Leader
     - All data reviewed will be published on the Section's PI page in the internal handbook ([example](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/). Sections may optionally provide supporting slides. 
     - Content will focus on the following areas:
     1. Development - Error Budgets, Past Due Security/InfraDev Issues (S1/S2)
     2. Quality - Past Due Bugs (S1/S2)
     3. UX - Past Due SUS Impacting Issues (S1/S2)
     4. Product - XMAU, XFN Prioritization
- **Recordings:** Each meeting will be recorded, and published so participants can engage async. The meetings have [REC] in the title and will automatically go to the [Google Drive Folder](https://drive.google.com/drive/search?q=type:video%20Performance%20Indicator). If you cannot find a recording, please message the EBA to the Product Division.
- **Preparation:** Section Leaders will provide the content at least 2 business days in advance of their review by posting a link to the updated PI pages and slides (optional) in the meeting agenda to allow ample time for meeting participants to review in advance. The Chief Product Officer may not attend the sync meeting but will always review async. 
- **Meeting Attendees** 
- The following participants will be included in the sync meeting from each group (attendance can be async):
   1. Engineering Managers or Sr Engineering Manager / Director for the stage
   2. Product Design Manager(s) for the Stage
   3. Quality Engineering Manager for the Stage
   4. Group Manager, Product for the Stage 
   5. Product Manager for the Group
- All Sections  will leverage and personalize this [template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/section-PI-review-monthly-template.md)
- All Sections will leverage and personalize this [agenda template](https://docs.google.com/document/d/1hKGU8hN_SpudlV-SnCu821EmMwgLVppJsXZmY4Gom4A/edit). 

#### Product Direction Showcases 
**DRIs:** [Director, Verify & Package](/company/team/#jreporter) and [Product Operations](/company/team/#fseifoddini)

- **What:** A monthly meeting for 4 stages to present 10-minutes on their direction as well as provide a deep dive or demo into a recent product delivery. 
- **When:** Monthly, rotating between EMEA and NA Timezone 
- **Duration:** 50 minutes
- **Format:** The meeting is lead by the Group Manager of the Stage. 
- **Recordings:** The meetings have [REC] in the title and will automatically go to the [Google Drive Folder](https://drive.google.com/drive/search?q=type:video%20Product%20Direction%20Showcase). If you cannot find a recording, please message the EBA to the Product Organization.
- **Preparation:** A monthly issue will get created and 2 weeks before meeting Stage leaders will populate their place in the meeting and will provide a link to a deck or direction page that will be reviewed for their timeslot. 

### Kickoff meetings

The purpose of our kickoff is to communicate with our community (both internal and external) a view of what improvements are being planned in the coming release. This can help other GitLab teams, community contributors and customers gain an understanding of what we are prioritizing and excited to deliver in our next iteration.

While kickoffs are commonly used to ensure visibility of work across an internal audience, our connection to a broader community and commitment to transparency means we also serve an external audience.

The process for preparing and presenting group and company wide Kickoff meetings is outlined in our [Monthly Kickoff issue template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Monthly-Kickoff.md). We create an issue from that template each month to collaborate on that month's Kickoff.

The calendar invite for the monthly kickoff meeting is scheduled for the 18th of each month. When the 18th falls on a Saturday or Sunday, the kickoff date is moved to the following Monday, which is either the 19th or 20th of the month.

### Product group counterparts

GitLab is designed and developed in a unique way.

Consistent with our value of [Efficiency](/handbook/values/#efficiency)
the product is developed with [directly responsible individuals](/handbook/people-group/directly-responsible-individuals/) from Product, UX, Quality and Development working together.

| Product Managers | Engineering Managers | UXers | SETs |
| :--------------: | :------------------: | :---: | :---: |
| Set [milestone priorities](/handbook/product/cross-functional-prioritization/#planning-for-the-milestone) and define what features Engineering works on | Own the definition of done; decide what gets merged into Product. [Prioritizes maintenance work](#prioritization-for-feature-maintenance-and-bugs) | Proactively identify small and large strategic UX needs to aid Product Management prioritization | Own and identify test strategy and requirements to complete the definition of done |

At GitLab, we develop our product for self-managed as well as SaaS-hosted customers. We realize that while we have DRIs there are many stakeholders who who must have input, including Engineering, Quality, UX, Product, Security, and Infrastructure. For example, the Security team often has the deeper context of what it takes to run a secure SaaS system. Similarly, the Infrastructure team has insights into what we should build into the product to reduce toil and enable efficient, reliable, performant, and scalable systems.

We call this the [Product Group](/company/team/structure/#product-groups) model. It is an extension of the classic quad concept at the leadership level and is currently comprised of Development, Quality, User Experience, Infrastructure, Product, and Security.

The Product Group can be used to facilitate a [global optimization](/handbook/values/#global-optimization), including product-wide [technical debt](/handbook/engineering/workflow/#technical-debt).

### Working with Product Management across the company 

There are many counterparts that PMs work with. Here are some best practices for working across the organization.

#### Working with Finance Business Partners 

In some cases, Product Managers may have items that incur expenses toward the budget. These can be related to external vendors for research, contractors for development staffing, and infrastructure. The CProdO is the DRI for the product budget and all changes or requests for budget spend must be approved through them. 

To request a forward-looking new budget item, open an issue in the Product project using the [Product Budget Request Template](https://gitlab.com/gitlab-com/Product/-/tree/main/.gitlab/issue_templates/Product-Budget-Request.md) and assign it to the CProdO and manager. Budgets are planned annually and quarterly, so approval may not be immediately given because it depends on the timing of budget planning. The CProdO will bring the budget request to the next budget planning session with Finance. 

To request approval for an increase in the expected spend for a pre-existing item, open an issue in the Product project using the [Product Budget Request Template](https://gitlab.com/gitlab-com/Product/-/tree/main/.gitlab/issue_templates/Product-Budget-Request.md) assign to the CProdO and tag your manager. The CProdO will review, approve or decline the budget change. The CProdO will then notify the Finance Business Partner of changes for forecast updates. 

### Working with Content Marketing

Content marketers and Product Managers can partner together when using a Blog to communicate product changes and engaging the market with thoughtful changes. See the [blog post handbook page](/handbook/marketing/brand-and-product-marketing/content/content-marketing/#blog-post) for guidelines on when and how to start engaging Content Marketing for creating a blog post for a feature.

### Working with Product Marketing (PMM)

Product marketers and managers should be joined at the hip. Just as a feature without documentation
should not be considered shipped, benefits of GitLab that we're not actively talking about might
as well not exist.

Product marketers rely on product managers to be guided to what is important and high impact.
In general, you should:

- always mention the [appropriate PMM](/handbook/product/categories/) on epics and high level issues
- regularly meet/talk async with the PMM that is aligned with your product area
- proactively reach out for input when contemplating new features
- involve PMM as early as possible with work on important changes

<%= partial "includes/usecase-competitive-content.md" %>

### Working with marketing 

When working on the release of larger features or new capabilities, it is important the product manager consider various aspects of the go to market plan and  inform or partner with the appropriate stable counterparts for strategic and logistical considerations. 

#### Marketing materials

As a PM you're responsible for making sure changes you've shipped are well represented
throughout GitLab's documentation and marketing materials. This means that on
release, [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/features.yml) is updated, documentation is merged and deployed, and
any existing content is updated where necessary.

It's not acceptable to do this after the release. GitLab is very complex, and features
and functions are easily missed, even those that provide significant value to customers
(e.g. the many ways you can authenticate with GitLab).

You can recruit the help of the marketing and technical writing team if needed,
but it's highly recommended to do small updates yourself. This takes less time
and overhead than communicating what needs to be done to someone else.

##### Pages that read from `features.yml`

It's important to keep [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/features.yml) updated because there are a number of different pages (internal-facing and external-facing) that read from that file. These include:

**External**

- [Pricing](/pricing/)
- [Features](/features/)
- [Why GitLab Premium?](/pricing/premium/)
- [Why GitLab Ultimate?](/pricing/ultimate)
- [Feature Comparison](/pricing/feature-comparison/)
- [DevOps Lifecycle](/stages-devops-lifecycle/)
- [DevOps Tools Landscape](/competition/)
- [DevOps Tools Comparisons](/competition/circleci/)

**Internal**

- [SaaS vs. Self-managed](/features/)
- [Features by tier](/features/by-paid-tier/)

### Working with User Experience (UX)

Product Managers and Product Designers should work together as strategic counterparts to better understand the problem and discover user needs. Product Designer will support the Product Manager in understanding the target audience, their challenges when using a particular feature and then designing a solution that helps users solve them.

It's important to remember that User Experience (UX) does not only relate to visual features or interface design. UX is the intangible design of a strategy that brings us to a solution, so it also refers to the experience of writing code, working with .yml files, designing APIs, working with a CLI, etc. All of those functionalities are meant to be read and used by people. Involving a Product Designer into their planning and development can be highly beneficial. A guide to consider is: anytime a person is interacting with something, there is an opportunity for that interaction to be designed.

##### Assessing user workflows

As the GitLab product matures, we know we must make important workflows easier to use. As part of this effort, the [UX department](/handbook/product/ux/) is collaborating with Product Management to select highly used workflows in stable areas of the product. We'll assess and score these workflows using the [UX Scorecards](/handbook/product/ux/ux-scorecards/) methodology and then work together to prioritize recommended improvements.

This effort aligns with the [Category Maturity](/direction/maturity/) goals that move areas of our product from minimal to viable to complete and, finally, lovable.

##### What if your team doesn't have a designer?

Product Designer assignments are listed in the team.yml file. Unfortunately, we are currently unable to assign a dedicated Product Designer for every group. Instead, Product Designers are assigned to the areas of highest business priority and will continue to provide focused support to those priorities throughout the year.

If there isn't a designer listed for a group, then that team is expected to be self-sufficient in taking responsibility for the design needs of their product area. Product Design does not have the capacity to review complex proposed design solutions or provide design solutions for unsupported groups. 

If you have questions or need support you can do so in the following ways:
- PMs who need to create designs can request access to Figma by creating an Access Request issue.     
- Review and follow the [Pajamas guidelines](https://design.gitlab.com/).
- If you have a small design question, or the Pajamas guidance is not clear, reach out via the `#ux` or `#ux_coworking` Slack channel.

