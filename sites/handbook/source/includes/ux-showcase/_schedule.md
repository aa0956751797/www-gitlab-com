[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Host                 | Presenter 1        | Presenter 2            | Presenter 3     |
| ---------- | -------------------- | ------------------ | ---------------------- | --------------- |
| 2023-05-31 | Justin Mandell       | Kevin Comoli       | Matt Nearents          |  Amelia Bauerly |
| 2023-06-14 | APAC                 | Alex Fracazo       | Katie Macoy            |                 |
| 2023-06-28 | Taurie Davis         | Dan Mizzi-Harris   | Austin Regnery         |  Nick Brandt    |
| 2023-07-12 | Chris Micek          | Emily Bauman       | Pedro Moreira da Silva |                 |
| 2023-07-26 | Andy Volpe           | Jeremy Elder       |                        |                 |
| 2023-08-09 | Rayana Verissimo     | Mike Nichols       |                        |                 |
| 2023-08-23 | Jacki Bauer          | Gina Doyle         | Veethika Mishra        |                 |
| 2023-09-06 | Marcel van Remmerden | Ali Ndlovu         | Sunjung Park           | Libor Vanc      |
| 2023-09-20 | Justin Mandell       | Nick Leonard       | Emily Sybrant          |                 |
| 2023-10-04 | APAC                 | Michael Le         | Alex Fracazo           |                 |
| 2023-10-18 | Rayana Verissimo     | Becka Lippert      | Nick Brandt            | Julia Miocene   |
| 2023-11-01 | Chris Micek          | Matt Nearents      | Kevin Comoli           |                 |
| 2023-11-15 | Andy Volpe           | Michael Fangman    | Jeremy Elder           |                 |
| 2023-11-29 | Rayana Verissimo     | Sascha Eggenberger | Camellia Yang          |                 |
| 2023-12-13 | Jacki Bauer          | Amelia Bauerly     | Emily Bauman           |                 |
