---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars in the month of June and July.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!


## June 2023

### AMER Time Zone Webinars

#### Advanced CI/CD
##### June 20th, 2023 at 12:00-1:00PM Eastern Time/4:00-5:00PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_5aDRd-AKRMuv8D85NNCEFA#)

#### DevSecOps/Compliance
##### June 27th, 2023 at 12:00-1:00PM Eastern Time/4:00-5:00PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_g9uXtLMsSRq2-i5R7MgEcw#)

#### Continuous Change Management in a Secure Way
##### June 28th, 2023 at 12:00-1:00PM Eastern Time/4:00-5:00PM UTC

Learn from our director of engineering how to shift left your change management process and why that is important. You will learn about the why and how of a safe and secure change management process.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_5nvvGPgUTtOYj19LpZUCGQ#)

### EMEA Time Zone Webinars

#### Advanced CI/CD
##### June 20th, 2023 at 10:00-11:00AM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_byI-hUZnQ5mATGG7eH4Srg)

#### DevSecOps/Compliance
##### June 22nd, 2023 at 10:00-11:00AM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_G5hkJv_TTwiVA0wFAVZI6w)

#### Getting Started with DevOps Metrics
##### June 27th, 2023 at 10:00-11:00AM UTC

Come learn about DevOps metrics in GitLab and why it is useful to track them. We will cover an overview of DORA metrics, Value Stream Analytics, and Insight dashboards, and what it looks like in Gitlab.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_e0LxX5o_QVuDqkqxiYxX8Q)

## July 2023

### AMER Time Zone Webinars

### Git Basics
##### July 7th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Are you new to Git? Attend this webinar targeted at beginners working with source code, where we will review the basics of using Git for version control and how it works with GitLab to help you get started quickly.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_bYSLBmvZS1O_46rwKMUEDA#)

#### Intro to GitLab
##### July 11th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

New to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_d_Lgdzw9SMmX-LXLJkMSWA#)

#### Intro to CI/CD
##### July 18th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_8__Dyj59RkWXvB5FtlNr5w#)

### Advanced CI/CD
##### July 25th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_xEnLCQgCRzedFJzrkoXy9A#)

#### DevSecOps/Compliance
##### July 31st, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_cvGqU4gaSeWDvUJp2lN02g#)

Check back later for more webinars! 
