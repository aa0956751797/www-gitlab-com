---
layout: handbook-page-toc
title: People Group Calendars
description: >-
  Calendars for team members and managers to help plan for the year at GitLab.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Calendars

1. [People Managers Calendar](https://about.gitlab.com/handbook/people-group/calendars/manager-calendar/)
1. [All Team Member Calendar](https://about.gitlab.com/handbook/people-group/calendars/team-member-calendar/)
